from django.apps import AppConfig


class MplateDecoderConfig(AppConfig):
    name = 'mplate_decoder'
