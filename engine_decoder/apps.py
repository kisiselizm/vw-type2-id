from django.apps import AppConfig


class EngineDecoderConfig(AppConfig):
    name = 'engine_decoder'
